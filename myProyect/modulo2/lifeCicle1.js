import React from 'react';
import { Text, View } from 'react-native';

export default class App extends React.Component {
	//Listing 2.15 static getDerivedStateFromProps
	state = {
		userLoggedIn: false
	}
	static getDerivedStateFromProps(nextProps, nextState) {
		if (nextProps.user.authenticated) {
			return {
				userLoggedIn: true
			}
		}
		return null
	}
	render() {
		return (
			<View style={styles.container}>
			{
				this.state.userLoggedIn && (
				<AuthenticatedComponent />
				)
			}
			</View>
		);
	}
}