import React from 'react'
import { Text, View } from 'react-native';
import MyComponent from './prop1_mycomponent'
class BookDisplay extends React.Component {
	render() {
		return (
		<View>
			<Text>{ this.props.book }</Text>
		</View>
		)
	}
}
export default BookDisplay;