import React from 'react'
import { Text, View } from 'react-native';
class MyComponent extends React.Component {
	//Listing 2.7 Displaying static props
	render() {
		let book = 'React Native in Action'
		return (
			<BookDisplay book={ book } />
		)
	}
}
class BookDisplay extends React.Component {
	render() {
		return (
		<View>
		<Text>{ this.props.book }</Text>
		</View>
		)
	}
}
export default BookDisplay;