import React from 'react'
import { Text, View } from 'react-native';
class MyComponent extends React.Component {
	//Listing 2.6 Static props
	
     constructor(){
        super()
		this.state = {
			year: 2016,
			leapYear: true,
			topics: ['React', 'React Native', 'JavaScript'],
			info: {
				paperback: true,
				length: '335 pages',
				type: 'programming'
			}
		}
  }
 	render() {
		let leapyear = <Text>This is not a leapyear!</Text>
		if (this.state.leapYear) {
			leapyear = <Text>This is a leapyear!</Text>
		}
		return (
			<View>
				<Text>{ this.state.year }</Text>
				<Text>Length: { this.state.info.length }</Text>
				<Text>Type: { this.state.info.type }</Text>
				{ leapyear }
			</View>
		)
	}
}
export default MyComponent;