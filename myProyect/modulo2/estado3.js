import React from 'react'
import { Text, View } from 'react-native';
class MyComponent extends React.Component {
	//Listing 2.4 Forcing rerender with forceUpdate
	//el link forceUpdate debe devolver el valor previo
	constructor(){
		super()
		this.state = {
			year: 2016
		}
	}
	updateYear() {
		this.setState({
		year: 2017
		})
	}
	update() {
		this.forceUpdate()
	}
	render() {
		return (
			<View>
				<Text onPress={ () => this.updateYear() }>
					The year is: { this.state.year }
				</Text>
				<Text onPress={ () => this.update () }>Force Update
				</Text>
			</View>
		)
	}
}
export default MyComponent;