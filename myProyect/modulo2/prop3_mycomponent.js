import React from 'react'
import { Text, View } from 'react-native';
class MyComponent extends React.Component {
	constructor() {
		super()
		this.state = {
		book: 'React Native in Action'
		}
	}
	render() {
		return (
		<BookDisplay book={this.state.book} />
		)
	}
}